# Verifier Specification

## What is the content of this repository?

A verifiable e-voting system requires a verifiable process and a verification software—the *verifier*—to verify the cryptographic evidence using data published on a private or public bulletin board. [Haenni et al.](https://e-voting.bfh.ch/app/download/8038993661/HDKL20.pdf) indicated that the specification and development of the verifier should go hand in hand with the e-voting solution, and the verifier challenges and extensively tests a protocol run.

The verifier allows an auditor to detect irregularities and is a crucial element for universal verifiability since—in principle— everyone could become an auditor. We group the verifications into four verification blocks that correspond to different phases of the protocol:

* Block 1: *VerifyConfigPhase*
* Block 2: *VerifyVotingPhase*
* Block 3: *VerifyOnlineTally*
* Block 4: *VerifyOfflineTally*

The verifier specification follows the same principles as the system and crypto-primitives specification:

* Mathematically precise pseudo-code algorithms
* Explicit domains of inputs and outputs
* Clear conventions for method and algorithm names

## Future work

We plan for the following improvements to the verifier specification:

* Extend the specification with additional verifications for tallying the results.
* Provide test vectors for specific algorithms.
* Specify the check that the correct number of voting cards were generated.
* Complete authenticity checks for all verification blocks.

## Change Log Release 0.9

Version 0.9 of the verifier specification is the initial published version.
